// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawn.h"

// Input
#include "InputMappingContext.h" 
#include "EnhancedInputSubsystems.h" 
#include "EnhancedInputComponent.h" 
#include "Components/BoxComponent.h"

// Components
#include "Components/ArrowComponent.h"

//Game logic
#include "Arcanoid/GameActors/Ball.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
APlayerPawn::APlayerPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create box
	BoxCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
	SetRootComponent(BoxCollision);

	// Create mesh
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(RootComponent);


	// Create arrow
	SpawnPoint = CreateDefaultSubobject<UArrowComponent>(TEXT("BallSpawnPoint"));
	SpawnPoint->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void APlayerPawn::BeginPlay()
{
	Super::BeginPlay();
	ABall* myBall = SpawnBall();
	if (myBall)
		myBall->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);
}

// Called every frame
void APlayerPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	MoveTick(DeltaTime);
}

// Called to bind functionality to input
void APlayerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	APlayerController* myPC = Cast<APlayerController>(GetController());
	UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(myPC->GetLocalPlayer());

	//clear mappings
	Subsystem->ClearAllMappings();
	Subsystem->AddMappingContext(InputMapping, 0);

	//get enhanced input components
	UEnhancedInputComponent* PEI = Cast<UEnhancedInputComponent>(PlayerInputComponent);

	//bind actions
	PEI->BindAction(MoveInput, ETriggerEvent::Triggered, this, &APlayerPawn::MoveInputStart);
	PEI->BindAction(MoveInput, ETriggerEvent::Completed, this, &APlayerPawn::MoveInputEnd);
	PEI->BindAction(StartInput, ETriggerEvent::Started, this, &APlayerPawn::StartBall);
}

void APlayerPawn::MoveInputStart(const FInputActionValue& Value)
{
	const float InputValue = Value.Get<float>();
	Direction.Y = InputValue;
}

void APlayerPawn::MoveInputEnd(const FInputActionValue& Value)
{
	Direction.Y = 0;
}

void APlayerPawn::MoveTick(float DeltaTime)
{
	FHitResult* Hit = nullptr;
	AddActorWorldOffset(Direction * Speed * DeltaTime, true, Hit);
}

ABall* APlayerPawn::SpawnBall()
{
	FVector SpawnLocation = SpawnPoint->GetComponentLocation();
	FRotator SpawnRotation = SpawnPoint->GetComponentRotation();
	FActorSpawnParameters SpawnParams;
	SpawnParams.Owner = this;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	ABall* NewBall = Cast<ABall>(GetWorld()->SpawnActor<AActor>(NextBallToSpawn, SpawnLocation, SpawnRotation, SpawnParams));
	if (NewBall)
	{
		NewBall->OnDestroyed.AddDynamic(this, &APlayerPawn::BallDestroyed);
		return NewBall;
	}
	return nullptr;
}

void APlayerPawn::BallDestroyed(AActor* Act)
{
	TArray<AActor*> Balls;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ABall::StaticClass(), Balls);
	if (Balls[0])
	{
		PlayerLives--;
		OnPlayerLiveTaken.Broadcast(PlayerLives);
		if (PlayerLives > 0)
		{
			ABall* NewBall = SpawnBall();
			if (NewBall)
				NewBall->InitBallMovement(FVector(1.0f, 0.f, 0.f));
		}
	}
}

void APlayerPawn::StartBall()
{
	if (!bBallIsActive)
	{
		ABall* myBall = Cast<ABall>(UGameplayStatics::GetActorOfClass(GetWorld(), ABall::StaticClass()));
		if (myBall)
		{
			myBall->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
			myBall->InitBallMovement(FVector(1.0f, 0.f, 0.f));
			bBallIsActive = true;
		}
	}

}

void APlayerPawn::BlockDestroyed(int32 PointsEarned)
{
	PlayerPoints += PointsEarned;
	OnPointsChanged.Broadcast(PlayerPoints);
}

void APlayerPawn::BatBonus_Implementation(bool Increase)
{
}

