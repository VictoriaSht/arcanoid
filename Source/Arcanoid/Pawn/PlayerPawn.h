// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"

#include "InputActionValue.h"

#include "PlayerPawn.generated.h"
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnPlayerLiveTaken, int32, CurrentLives);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnPointsChanged, int32, ChangeValue);

UCLASS()
class ARCANOID_API APlayerPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawn();

	UPROPERTY(BlueprintAssignable)
	FOnPlayerLiveTaken OnPlayerLiveTaken;
	UPROPERTY(BlueprintAssignable)
	FOnPointsChanged OnPointsChanged;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//*		COMPONENTS	 *//
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Components)
	class UArrowComponent* SpawnPoint = nullptr;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Components)
	class UStaticMeshComponent* Mesh = nullptr;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Components)
	class UBoxComponent* BoxCollision = nullptr;


	//*		INPUT	 *//
	//
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Enhanced Input")
	class UInputMappingContext* InputMapping;

	// InputActions
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* MoveInput;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* StartInput;

	// Bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;


	//*		MOVEMENT	 *//
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
	float Speed = 100;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ForLogic")
	FVector Direction = FVector(0);
	void MoveInputStart(const FInputActionValue& Value);
	void MoveInputEnd(const FInputActionValue& Value);
	void MoveTick(float DeltaTime);


	//* BALL LOGIC *//
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
	TSubclassOf<class ABall> NextBallToSpawn = nullptr;
	ABall* SpawnBall();
	UFUNCTION()
	void BallDestroyed(AActor* Act);
	void StartBall();

	bool bBallIsActive = false;

	//* GAME RULES *//
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
	int32 PlayerLives = 3;
	int32 PlayerPoints = 0;

	UFUNCTION()
	void BlockDestroyed(int32 PointsEarned);

	//* BONUSES *//
	UFUNCTION(BlueprintNativeEvent)
	void BatBonus(bool Increase);
};
