// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "ArcanoidGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class ARCANOID_API UArcanoidGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	int32 PlayerRecordPoints = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<int32> CompletedLvls;
	
};
