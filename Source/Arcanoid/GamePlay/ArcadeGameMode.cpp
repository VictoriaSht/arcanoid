// Fill out your copyright notice in the Description page of Project Settings.


#include "ArcadeGameMode.h"
#include "ArcanoidGameInstance.h"
#include "Arcanoid/Pawn/PlayerPawn.h"
#include "Kismet/GameplayStatics.h"

void AArcadeGameMode::BeginPlay()
{
	Super::BeginPlay();

	APlayerPawn* myPawn = Cast<APlayerPawn>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
	if (myPawn)
		myPawn->OnPlayerLiveTaken.AddDynamic(this, &AArcadeGameMode::PlayerLiveIsTaken);
}

void AArcadeGameMode::LevelCleared()
{
	APlayerPawn* myPawn = Cast<APlayerPawn>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
	UArcanoidGameInstance* myGI = Cast<UArcanoidGameInstance>(GetGameInstance());
	if (myGI && myPawn)
	{
		int32 PreviousHighScore = myGI->PlayerRecordPoints;
		myGI->PlayerRecordPoints = myPawn->PlayerPoints;
		LevelClearedBP(PreviousHighScore, myGI->PlayerRecordPoints);
	}
}

void AArcadeGameMode::LevelFailed()
{
}

void AArcadeGameMode::PlayerLiveIsTaken(int32 CurrentLives)
{
	if (CurrentLives <= 0)
	{
		APlayerPawn* myPawn = Cast<APlayerPawn>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
		UArcanoidGameInstance* myGI = Cast<UArcanoidGameInstance>(GetGameInstance());
		if (myGI && myPawn)
		{
			int32 PreviousHighScore = myGI->PlayerRecordPoints;
			myGI->PlayerRecordPoints = myPawn->PlayerPoints;
			LevelFailedBP(PreviousHighScore, myGI->PlayerRecordPoints);
		}
	}
}

void AArcadeGameMode::LevelFailedBP_Implementation(int32 PreviousHighScore, int32 CurrentHighScore)
{
}

void AArcadeGameMode::LevelClearedBP_Implementation(int32 PreviousHighScore, int32 CurrentHighScore)
{
}
