// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ArcadeGameMode.generated.h"

/**
 * 
 */
UCLASS()
class ARCANOID_API AArcadeGameMode : public AGameModeBase
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;
	
public:
	// Called from BlockGenerator
	UFUNCTION()
	void LevelCleared();

	// Called from player pawn
	UFUNCTION()
	void LevelFailed();

	UFUNCTION()
	void PlayerLiveIsTaken(int32 CurrentLives);

	UFUNCTION(BlueprintNativeEvent)
	void LevelClearedBP(int32 PreviousHighScore, int32 CurrentHighScore);
	UFUNCTION(BlueprintNativeEvent)
	void LevelFailedBP(int32 PreviousHighScore, int32 CurrentHighScore);
};
