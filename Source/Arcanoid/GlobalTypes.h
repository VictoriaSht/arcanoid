// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "GlobalTypes.generated.h"

UENUM(BlueprintType)
enum class EBlockType : uint8
{
	NONE UMETA(DisplayName = "NONE_Block"),
	RegularBlock UMETA(DisplayName = "RegularBlock"),
	HardBlock UMETA(DisplayName = "HardBlock"),
	ExplosiveBlock UMETA(DisplayName = "ExplosiveBlock"),
	GuaranteedBonus UMETA(DisplayName = "GuaranteedBonus")
};

UENUM(BlueprintType)
enum class EBonus : uint8
{
	NONE UMETA(DisplayName = "NONE"),
	BatPlusSize UMETA(DisplayName = "BatPlusSize"),
	BatMinusSize UMETA(DisplayName = "BatMinusSize"),
	BallPlusSize UMETA(DisplayName = "BallPlusSize"),
	BallMinusSize UMETA(DisplayName = "BallMinusSize"),
	NoFail UMETA(DisplayName = "NoFail"),
	PerforatingBall UMETA(DisplayName = "PerforatingBall")
};

USTRUCT(BlueprintType)
struct FBlocksPatterns : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<EBlockType> BlockRowArray;
};

/**
 * 
 */
class ARCANOID_API GlobalTypes
{
public:
	GlobalTypes();
	~GlobalTypes();
};
