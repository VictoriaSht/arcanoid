// Fill out your copyright notice in the Description page of Project Settings.


#include "BlockGenerator.h"
#include "Block.h"
#include "Kismet/GameplayStatics.h"
#include "Arcanoid/GamePlay/ArcadeGameMode.h"

// Sets default values
ABlockGenerator::ABlockGenerator()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABlockGenerator::BeginPlay()
{
	Super::BeginPlay();

	AArcadeGameMode* myGM = Cast<AArcadeGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (myGM)
	{
		OnLevelCleared.AddDynamic(myGM, &AArcadeGameMode::LevelCleared);
	}
}

// Called every frame
void ABlockGenerator::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABlockGenerator::OnConstruction(const FTransform& Transform)
{
	TArray<AActor*> BlockActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ABlock::StaticClass(), BlockActors);
	for (AActor* elem : BlockActors)
	{
		elem->Destroy();
	}

	FVector ActorLocation = GetActorLocation();
	FRotator ActorRotation = GetActorRotation();
	FActorSpawnParameters SpawnParams;
	SpawnParams.Owner = this;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	if (DT_BlockPattern)
	{
		TArray<FName> RowNames = DT_BlockPattern->GetRowNames();
		for (int i = 0; i < RowNames.Num(); i++)
		{
			FBlocksPatterns* BlockRow = DT_BlockPattern->FindRow<FBlocksPatterns>(RowNames[i], "");
			for (int j = 0; j < BlockRow->BlockRowArray.Num(); j++)
			{
				if (BlockRow->BlockRowArray.IsValidIndex(j))
				{
					//UE_LOG(LogTemp, Warning, TEXT("ABlockGenerator::ABlockGenerator - class %s"), BlockRow->BlockRowArray[j]);
					TSubclassOf<ABlock>* BlockClass = BlockTypes.Find((BlockRow->BlockRowArray[j]));
					if (BlockClass)
					{
						FVector SpawnLocation = FVector(ActorLocation.X + (SpawnOffset * i), ActorLocation.Y + (SpawnOffset * j), ActorLocation.Z);
						ABlock* NewBlock = GetWorld()->SpawnActor<ABlock>((*BlockClass), SpawnLocation, ActorRotation, SpawnParams);
						if (NewBlock)
						{
							NewBlock->OnDestroyed.AddDynamic(this, &ABlockGenerator::OnBlockDestroyed);

							if (bMovingBlocks)
							{
								NewBlock->PrimaryActorTick.bCanEverTick = true;
								NewBlock->SetActorTickInterval(2.0f);
								if (i % 2 == 0)
									NewBlock->Tags.Add(FName("Right"));
							}
						}
					}
				}
			}
		}
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("ABlockGenerator::ABlockGenerator - PatternDataTable -NULL"));
}

void ABlockGenerator::OnBlockDestroyed(AActor* Act)
{
	
	TArray<AActor*> BlockActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ABlock::StaticClass(), BlockActors);
	if (BlockActors.Num() == 1)
	{
		// Last block is not destroyed yet
		OnLevelCleared.Broadcast();
	}
}

