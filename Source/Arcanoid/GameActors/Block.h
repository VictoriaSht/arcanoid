// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Block.generated.h"
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnBlockDestroyed, int32, EarnedPoints);

UCLASS()
class ARCANOID_API ABlock : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABlock();

	FOnBlockDestroyed OnBlockDestroyed;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Components
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Components)
	class UStaticMeshComponent* Mesh;

	// Bonus BP class
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ForLogic")
	TSubclassOf<class AArcanoid_Bonus> BonusBPClass;

	// Materials for states
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
	TMap<int32, UMaterialInterface*> StateMaterials;
	// Destroy FX
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
	UParticleSystem* DestroyEffect = nullptr;

	// Stats
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
	int32 MyLives = 3;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
	int32 MyPoints = 100;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
	float BonusChance = 0.2f;

	void OneLiveTaken(AActor* DamageCauser);

	UFUNCTION(BlueprintCallable)
	void DestroyBlock();

	UFUNCTION()
	void MeshCollisionHit(UPrimitiveComponent* HitComponent, AActor* OtherActor,
		UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	UFUNCTION()
	void MeshBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
		UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	// Movement
	void Move();
	float ElementSize = 120.0f;
	int32 StepCount = 0;

};
