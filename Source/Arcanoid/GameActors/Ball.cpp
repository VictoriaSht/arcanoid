// Fill out your copyright notice in the Description page of Project Settings.


#include "Ball.h"
#include "Math/UnrealMathUtility.h"
#include "Kismet/GameplayStatics.h"
#include "Arcanoid/Pawn/PlayerPawn.h"
#include "Components/SphereComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"

// Sets default values
ABall::ABall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create sphere collision
	Sphere = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere Collision"));
	Sphere->OnComponentHit.AddDynamic(this, &ABall::OnHit);
	SetRootComponent(Sphere);

	// Add ball tag
	Tags.Add(FName("Ball"));
}

// Called when the game starts or when spawned
void ABall::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	MovementTick(DeltaTime);

}

void ABall::MovementTick(float DeltaTime)
{
	FHitResult* HitResult = nullptr;
	AddActorLocalOffset(Direction * Speed * DeltaTime, true, HitResult);
}

void ABall::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	// Play hit sound
	EPhysicalSurface mySurfaceType = UGameplayStatics::GetSurfaceType(Hit);
	if (HitSounds.Contains(mySurfaceType))
	{
		USoundBase* mySound = HitSounds[mySurfaceType];
		if (mySound)
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), mySound, Hit.ImpactPoint);
	}

	// Spawn Hit Effect
	if (HitEffects.Contains(mySurfaceType))
	{
		UNiagaraSystem* myEffect = HitEffects[mySurfaceType];
		if (myEffect)
			UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), myEffect, Hit.ImpactPoint, Hit.ImpactNormal.Rotation());
	}

	// Get player direction data
	FVector PlayerDir = FVector(0.0f);
	APlayerPawn* myPawn = Cast<APlayerPawn>(OtherActor);
	if (myPawn)
		PlayerDir = myPawn->Direction;

	// Reflect direction on hit, keep Z
	FVector ReflectionVector = FMath::GetReflectionVector(Direction, Hit.ImpactNormal) + (PlayerDir * PlayerDirMultiplier);
	Direction = FVector(ReflectionVector.X, ReflectionVector.Y, 0);
	Direction.Normalize(0.0005f);

	// Make sure we dont stuck
	if (Direction.IsNearlyZero())
		Direction = FVector(1.0f, 0.f, 0.f);
	
	//// To not stuck between two plain walls old method
	//if (Direction.Equals(Hit.ImpactNormal, 1.0f))
	//{
	//	float rand = FMath::RandRange(-0.1f, 0.1f);
	//	Direction = FVector(Direction.X + rand, Direction.Y + rand, 0);
	//	Direction.Normalize();
	//}

	// Make sure we dont stuck between two plain walls on X coordinate
	if (Direction.X < 0.1f)
	{
		float rand = FMath::RandRange(-0.2f, 0.2f);
		Direction.X += rand;
	}
	
	// Make sure we dont stuck between two plain walls on Y coordinate
	if (Direction.Y < 0.1f)
	{
		float rand = FMath::RandRange(-0.2f, 0.2f);
		Direction.Y += rand;
	}

	//UE_LOG(LogTemp, Warning, TEXT("HitActor: %s"), *OtherActor->GetName());
}

void ABall::InitBallMovement(FVector newDirection)
{
	Direction = newDirection;
}
void ABall::PerforatingBonusEnd_Implementation()
{
}
void ABall::PerforatingBonus_Implementation()
{
}


void ABall::BallBonus_Implementation(bool Increase)
{
}

