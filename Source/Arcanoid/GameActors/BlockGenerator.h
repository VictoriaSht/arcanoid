// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Arcanoid/GlobalTypes.h"
#include "BlockGenerator.generated.h"
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnLevelCleared);

UCLASS()
class ARCANOID_API ABlockGenerator : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABlockGenerator();

	FOnLevelCleared OnLevelCleared;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void OnConstruction(const FTransform& Transform) override;

	// Pattern data table
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UDataTable* DT_BlockPattern = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bMovingBlocks = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TMap<EBlockType, TSubclassOf<class ABlock>> BlockTypes;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float SpawnOffset = 200.0f;

	UFUNCTION()
	void OnBlockDestroyed(AActor* Act);
};
