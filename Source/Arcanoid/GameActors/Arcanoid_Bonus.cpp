// Fill out your copyright notice in the Description page of Project Settings.


#include "Arcanoid_Bonus.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Arcanoid/Pawn/PlayerPawn.h"
#include "Ball.h"

// Sets default values
AArcanoid_Bonus::AArcanoid_Bonus()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


	// Create sphere collision...
	SphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere Collision"));
	SphereCollision->OnComponentBeginOverlap.AddDynamic(this, &AArcanoid_Bonus::SphereOnBeginOverlap);
}

// Called when the game starts or when spawned
void AArcanoid_Bonus::BeginPlay()
{
	Super::BeginPlay();

	if (ObjectBonus == EBonus::NONE)
		InitRandomBonus();
	else
		InitPresetBonus();
}

// Called every frame
void AArcanoid_Bonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AArcanoid_Bonus::InitRandomBonus()
{
	uint8 rand = FMath::RandRange(static_cast<int>(EBonus::BatPlusSize), static_cast<int>(EBonus::PerforatingBall));
	ObjectBonus = static_cast<EBonus>(rand);
	InitObjectBP(ObjectBonus);
}

void AArcanoid_Bonus::InitPresetBonus()
{
	InitObjectBP(ObjectBonus);
}

void AArcanoid_Bonus::BallBonus(bool Increase)
{
	TArray<AActor*> Balls;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ABall::StaticClass(), Balls);
	for (AActor* elem : Balls)
	{
		ABall* myBall = Cast<ABall>(elem);
		if (myBall)
			myBall->BallBonus(Increase);
	}
}

void AArcanoid_Bonus::BatBonus(class APlayerPawn* Pawn, bool Increase)
{
	Pawn->BatBonus(Increase);
}

void AArcanoid_Bonus::PerforatingBall()
{
	TArray<AActor*> Balls;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ABall::StaticClass(), Balls);
	for (AActor* elem : Balls)
	{
		ABall* myBall = Cast<ABall>(elem);
		if (myBall)
			myBall->PerforatingBonus();
	}
}

void AArcanoid_Bonus::NoFailBonusEvent_Implementation()
{
}

void AArcanoid_Bonus::InitObjectBP_Implementation(EBonus NewBonus)
{
}

void AArcanoid_Bonus::SphereOnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	APlayerPawn* myPawn = Cast<APlayerPawn>(OtherActor);
	if (myPawn)
	{
		switch (ObjectBonus)
		{
		case EBonus::BatPlusSize:
			BatBonus(myPawn, true);
			break;
		case EBonus::BatMinusSize:
			BatBonus(myPawn, false);
			break;
		case EBonus::BallPlusSize:
			BallBonus(true);
			break;
		case EBonus::BallMinusSize:
			BallBonus(false);
			break;
		case EBonus::PerforatingBall:
			PerforatingBall();
			break;
		case EBonus::NoFail:
			NoFailBonusEvent();
			break;
		default:
			break;
		}

		Destroy();
	}

}

