// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Ball.generated.h"

UCLASS()
class ARCANOID_API ABall : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABall();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// For adjusting direction

	FVector Direction = FVector(0.0f, 0.0f, 0.0f);
	inline static float PlayerDirMultiplier = -0.5f;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void MovementTick(float DeltaTime);

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	class USphereComponent* Sphere = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
	float Speed = 100;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
	TMap<TEnumAsByte<EPhysicalSurface>, USoundBase*> HitSounds;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
	TMap<TEnumAsByte<EPhysicalSurface>, class UNiagaraSystem*> HitEffects;

	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	void InitBallMovement(FVector newDirection);

	//* Bonuses *//
	UFUNCTION(BlueprintNativeEvent)
	void BallBonus(bool Increase);

	UFUNCTION(BlueprintNativeEvent)
	void PerforatingBonus();

	UFUNCTION(BlueprintNativeEvent)
	void PerforatingBonusEnd();
};
