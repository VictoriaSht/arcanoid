// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Arcanoid/GlobalTypes.h"
#include "Arcanoid_Bonus.generated.h"

UCLASS()
class ARCANOID_API AArcanoid_Bonus : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AArcanoid_Bonus();

	// Collision sphere
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Components)
	class USphereComponent* SphereCollision;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EBonus ObjectBonus = EBonus::NONE;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Init Object
	void InitRandomBonus();
	void InitPresetBonus();
	UFUNCTION(BlueprintNativeEvent)
	void InitObjectBP(EBonus NewBonus);


	// Bonuses implementation
	void BatBonus(class APlayerPawn* Pawn, bool Increase);
	void BallBonus(bool Increase);
	void PerforatingBall();

	UFUNCTION(BlueprintNativeEvent)
	void NoFailBonusEvent();

	UFUNCTION()
	void SphereOnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
		UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

};
