// Fill out your copyright notice in the Description page of Project Settings.

#include "Block.h"
#include "Components/StaticMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Arcanoid_Bonus.h"
#include "Arcanoid/Pawn/PlayerPawn.h"
#include "Kismet/KismetMathLibrary.h"


// Sets default values
ABlock::ABlock()
{
	// Create root component
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

	// Create a mesh...
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->OnComponentHit.AddDynamic(this, &ABlock::MeshCollisionHit);
	Mesh->OnComponentBeginOverlap.AddDynamic(this, &ABlock::MeshBeginOverlap);
	Mesh->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void ABlock::BeginPlay()
{
	Super::BeginPlay();
	
	APlayerPawn* PlayerPawn = Cast<APlayerPawn>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
	if (PlayerPawn)
		OnBlockDestroyed.AddDynamic(PlayerPawn, &APlayerPawn::BlockDestroyed);

	if (ActorHasTag(FName("Right")))
		ElementSize = -ElementSize;
}

// Called every frame
void ABlock::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();

	//UE_LOG(LogTemp, Warning, TEXT("%f"), MovementMultiplier);

}

void ABlock::OneLiveTaken(AActor* DamageCauser)
{
	if (DamageCauser->ActorHasTag("Ball"))
	{
		MyLives--;

		if (MyLives <= 0)
		{
			DestroyBlock();
		}
		else
		{
			UMaterialInterface* myMaterial = (*StateMaterials.Find(MyLives));
			if (myMaterial)
				Mesh->SetMaterial(0, myMaterial);
		}
	}
}

void ABlock::DestroyBlock()
{
	// Tell player to add points
	OnBlockDestroyed.Broadcast(MyPoints);

	// Spawn destroy effect
	if (DestroyEffect)
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), DestroyEffect, GetActorTransform());

	// Spawn bonus with some chance
	bool bBonus = UKismetMathLibrary::RandomBoolWithWeight(BonusChance);
	if (bBonus)
	{
		FVector SpawnLocation = GetActorLocation();
		FRotator SpawnRotation = GetActorRotation();
		FActorSpawnParameters SpawnParams;
		SpawnParams.Owner = this;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
		GetWorld()->SpawnActor<AArcanoid_Bonus>(BonusBPClass, SpawnLocation, SpawnRotation, SpawnParams);
	}

	Destroy();
}

void ABlock::MeshCollisionHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	OneLiveTaken(OtherActor);
}

void ABlock::MeshBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	OneLiveTaken(OtherActor);
}

void ABlock::Move()
{
	FVector MovementVector(ForceInitToZero);

	switch (StepCount)
	{
	case 0:
		MovementVector.Y += ElementSize;
		StepCount++;
		break;
	case 1:
		MovementVector.Y -= ElementSize;
		StepCount++;
		break;
	case 2:
		MovementVector.Y -= ElementSize;
		StepCount++;
		break;
	case 3:
		MovementVector.Y += ElementSize;
		StepCount = 0;
		break;
	default:
		break;
	}
	//UE_LOG(LogTemp, Warning, TEXT("%s"), *FString::FromInt(StepCount));

	AddActorWorldOffset(MovementVector);
}